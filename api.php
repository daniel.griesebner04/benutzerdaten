<?php
require_once "lib/db.data.php";

//Überprüfung ob GET Parameter gesetzt ist
$search = $_GET['search'] ?? null;

//Wenn getParameter den Wert getALL hat werden alle zurückgeliefert sonst nur mit dem Filter
if($search == "getAll"){
    echo json_encode(getAllData());
}elseif($search != null){
    echo json_encode(getFilteredData($search));
}


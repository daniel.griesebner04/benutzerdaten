//Keypup Event -> Tabelle Filtern
$("#search").keyup(function( event ) {
    filterRows();
});

//on Document Ready - wenn das Dokument geladen hat
$(document).ready(function (){
    recieveData("getAll");
    filterRows();
});

function recieveData(search){
    let data = "search=" + search;
    //Asynchrone Anfrage an Server - Seite wird nicht neu geladen
    let request = $.ajax({
        url: "api.php",
        type: "get",
        data: data
    });

    //Wenn Daten vom Server zum Client übermittelt worden sind
    request.done(function (json) {
        //Tabelle erstellen
        makeTable(JSON.parse(json));
        //Tabelle Filtern, falls etwas in dem Textfeld steht
        filterRows();
    });
}

//Tabelle erstellt
function makeTable(data) {
    let table = $("#benutzer");
    table.empty();
    $.each(data, function(rIndex, rowData) {
        let row = $("<tr/>");
        //Link erstellen
        let link = $('<a>', {
            text: rowData['firstname'] + " " + rowData['lastname'],
            href: "detail.php?id=" + rowData['id']
        });
        //Link Reihe hinzufügen
        row.append($("<td/>").append(link));
        row.append($("<td/>").text(rowData['email']));
        let date = new Date(rowData['birthdate']);
        let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(date);
        let mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(date);
        let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);
        row.append($("<td/>").text(`${da}.${mo}.${ye}`));
        //Reihe in Tabelle
        table.append(row);
    });
}

function filterRows() {
    var filter = $("#search").val().toLowerCase();


    if (filter === "") {
        noresult.hide();
        $('#benutzer tr').each(function (i, row) {

            $(row).show();

        });
    } else {

        $('#benutzer tr').each(function (i, row) {
            // ignore header row

                var $row = $(row);  // convert to object

                var name = $row.find('td:nth-child(1)').text().toLowerCase();
                var email = $row.find('td:nth-child(2)').text().toLowerCase();

                if (name.indexOf(filter) >= 0 || email.indexOf(filter) >= 0) {
                    $row.show();

                } else {
                    $row.hide();
                }
        });
    }
}
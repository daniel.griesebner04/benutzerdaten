<?php

require_once "lib/db.data.php";

    $getID = $_GET['id'];
    if(!isset($getID)){
        die("Diese ID gibt es nicht");
    }else{


?>
    <!doctype HTML>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Benutzerdaten</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </head>
    <body>

    <div class="container">
        <h1 class="mt-3">Benutzerdetails</h1>

        <?php
            //Datenbeschaffung von Server mit ID
            $person = getDataPerId($getID);
        ?>
        <a href="javascript:history.back()">Zurück</a>
        <br>
        <br>
        <div class="row">
            <div class="col-sm-12">

                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Vorname</td>
                        <td><?= $person['firstname']?></td>
                    </tr>
                        <tr>
                            <td>Nachname</td>
                            <td><?= $person['lastname']?></td>
                        </tr>
                        <tr>
                            <td>Geburtsdatum</td>
                            <td><?= date_format(date_create($person['birthdate']), "d.m.Y")?></td>
                        </tr>
                        <tr>
                            <td>E-Mail</td>
                            <td><?= $person['email']?></td>
                        </tr>
                        <tr>
                            <td>Telefon</td>
                            <td><?= $person['phone']?></td>
                        </tr>
                        <tr>
                            <td>Straße</td>
                            <td><?= $person['street']?></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>

    </div>

    </body>
    </html>
<?php
}
?>

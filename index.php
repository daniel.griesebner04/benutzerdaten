<?php

require_once "lib/func.inc.php";


$searchText = $_POST['search'] ?? "";
?>
<!doctype HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Benutzerdaten</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>


</head>
<body>



<div class="container">
    <h1 class="mt-3">Benutzerdaten anzeigen</h1>


    <form method="POST" action="index.php" id="searchForm">
        <div class="row form-group mt-3">
            <div class="col-sm-1">
                <label class="col-form-label" for="search">Suche:</label>
            </div>
            <div class="col-sm-5">
                <input class="form-control" type="text" placeholder="Suchtext" id="search" name="search" value="<?= htmlspecialchars($searchText)?>"/>
            </div>
            <div class="col-sm-6">
                <input type="submit" name="suche" id="suche" class="btn btn-primary" />

                <a href="index.php" class="btn btn-secondary">Leeren</a>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-sm-12">
            <p id="noresult" class="alert alert-danger" style="display: none;">Kein Ergebnis</p>
            <table class="table table-striped" >
                <thead>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Geburtsdatum</th>
                </thead>
                <tbody id="benutzer">

                </tbody>
            </table>

        </div>
    </div>
</div>
<script src="js/index.js"></script>
</body>
</html>

<?php
require_once "PHP-13 userdata.php";

global $data;

function getAllData() : array{
    global $data;
    return $data;
}

function getDataPerId($id) : ?array{
    global $data;
    foreach ($data as $d){
        if($d['id'] == $id){
            return $d;
        }
    }
    return null;
}

function getFilteredData($filter) :array{
    global $data;
    $result = array();
    foreach ($data as $d){
        $name = $d['firstname'] . " " . $d['lastname'];
        $email = $d['email'];
        $birthdate = $d['birthdate'];
        if(str_contains($name, $filter) || str_contains($email, $filter) || str_contains($birthdate, $filter)){
            array_push($result, $d);
        }
    }
    
    
    return $result;
}
